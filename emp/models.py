from django.db import models

class employee(models.Model):
    name=models.CharField(max_length=25)
    depno=models.IntegerField()
    email=models.EmailField()
    def __str__(self):
        return f'name:{self.name} depno:{self.depno} email:{self.email}'
class salary(models.Model):
    acno=models.IntegerField()
    sal=models.IntegerField()
    emp=models.ForeignKey(employee,on_delete=models.CASCADE)
    def __str__(self):
        return f'acno:{self.acno} salary:sal{self.sal}'
