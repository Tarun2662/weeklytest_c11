from django.shortcuts import render,get_list_or_404,get_object_or_404,redirect
from .models import employee,salary

def emp_list(request):
    emp1=get_list_or_404(employee)
    return render(request,'emp/list.html',{'emps':emp1})
def emp_details(request ,depno_):
    emp2=get_object_or_404(employee ,depno=depno_)
    emp5=salary.objects.filter(emp=emp2)
    if emp5 is not None:
        return render(request,'emp/detail.html',{'emps':emp2,'sala':emp5})
    else:
        return render(request,'emp/detail.html',{'emps':emp2})
def emp_create(request):
    if request.method=='GET':
        return render(request,'emp/admission.html')
    if request.method=='POST':
        n=request.POST.get('name','')
        d=request.POST.get('depno',0)
        e=request.POST.get('email','')
        emp3=employee.objects.create(name=n,depno=d,email=e)
        emp3.save()
        return redirect('emp_list')
def emp_delete(request,id_):
    emp4=get_object_or_404(employee ,pk=id_)
    emp4.delete()
    return redirect('emp_list')
def emp_update(request,id_):
    if request.method=='GET':
        context={}
        emp=get_object_or_404(employee,pk=id_)
        context['emps']=emp
        return render(request,'emp/update.html',context)
    if request.method=='POST':
        emp=get_object_or_404(employee,pk=id_)
        n=request.POST.get('name','')
        d=request.POST.get('depno',0)
        e=request.POST.get('email','')
        emp.name=n
        emp.depno=d
        emp.email=e
        emp.save()
        return redirect('emp_list')

        

        

    
        



