from django.urls import path
from .import views
urlpatterns = [path('list',views.emp_list,name='emp_list'),
                path('employee/<int:depno_>', views.emp_details, name='emp_details'),
                path('add',views.emp_create,name='emp_create'),
                path('delete/<int:id_>',views.emp_delete,name="emp_delete"),
                path('update/<int:id_>',views.emp_update,name='emp_update')]