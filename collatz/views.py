from django.shortcuts import render
from .models import student


def collatzseq(request):
    if request.method =='GET':
        return render(request,'collatz/interface.html')
    if request.method=='POST':
        no=request.POST.get('n',0)
        no= int(no)
        l=[]
        while no!=1:
            l.append(no)
            if no%2==0:
                no=no//2
            else:
                no=3*no +1
        context={'seq':l}
        return render(request,'collatz/interface.html',context)
    
def list_view(request):
    students=student.objects.all()
    context={"student":students}
    return render(request,'collatz/list.html',context)
def details(request ,roll):
    val=student.objects.get(roll_no=roll)
    context={"detail":val}
    return render(request ,'collatz/details.html',context)