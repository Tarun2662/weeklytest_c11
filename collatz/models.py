from django.db import models

class student(models.Model):
    name=models.CharField(max_length=25)
    roll_no=models.IntegerField()
    def __str__(self):
        return f'{self.roll_no}:{self.name}'

