from django.db import models

class notice(models.Model):
    tittle=models.CharField(max_length=30)
    text=models.CharField(max_length=256)
    contact=models.IntegerField()
    
    def __str__(self) -> str:
        return f'title:{self.tittle} text:{self.text} contact:{self.contact}'
