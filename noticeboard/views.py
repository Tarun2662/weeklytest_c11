from django.shortcuts import render,get_list_or_404,get_object_or_404,redirect
from .models import notice

def notice_input(request):
    
    if request.method=='GET':
        return render(request,'noticeboard/input.html')
    if request.method=='POST':
        t=request.POST.get('title' ,'')
        te=request.POST.get('text','')
        c=request.POST.get('contact',0)
        x=notice.objects.count()
        
        if x>=6:
            
            Z=notice.objects.filter(id__in=list(notice.objects.values_list('pk', flat=True)[:1]))
            Z.delete()
            
        
        n=notice.objects.create(tittle=t, text=te,contact=c)

        n.save()
        return redirect('notice_output')
def notice_output(request):
    k=get_list_or_404(notice)
    context={'note':k}
    return render(request,'noticeboard/notice.html',context)




